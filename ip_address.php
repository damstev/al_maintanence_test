<?php
/**
 * Get the number of netmask bits from a netmask in presentation format
 *
 * @param string $netmask Netmask in presentation format
 *
 * @return integer Number of mask bits
 * @throws Exception
 */
function netmask2bitmask($ip)
{
  if(!is_string($ip))
    throw new Exception('The input is not a valid IP address');

  if(validIPv4($ip)){
    return IPv4Calculator($ip);
  }elseif(validIPv6($ip)){
    return IPv6Calculator($ip);
  }

  throw new Exception('The input is not a valid IP address');

}

function validIPv4($input){
  return false !== filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
}

function validIPv6($input){
  return false !== filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
}

function IPv4Calculator($ip){
  $long = ip2long($ip);
  $base = ip2long('255.255.255.255');
  $cidr = 32-log(($long ^ $base)+1,2);
  if(is_numeric( $cidr ) && floor( $cidr ) !== $cidr)
    throw new Exception("The input $ip is not a valid IPv4 netmask");
  return $cidr;
}

function IPv6Calculator($ip) {
    $binNum = '';
    foreach (unpack('C*', inet_pton($ip)) as $byte) {
        $binNum .= str_pad(decbin($byte), 8, "0", STR_PAD_LEFT);
    }
    // return $binNum;
    // return base_convert(ltrim($binNum, '0'), 2, 10);

    $totalOnesCount = substr_count($binNum, '1');
    $count = 0;
    ///count the continous ones from the begining, stop when find anyting else
    for ($i=0; $i < strlen($binNum); $i++) {
      if('1' == $binNum[$i])
          $count++;
      else
          break;
    }

    ///netmask are composed for continous ones followed for none or more zeros
    if($totalOnesCount !== $count)
      throw new Exception("The input $ip is not a valid IPv6 netmask");

    return $count;
}

try{
  echo netmask2bitmask('255.255.0.0') . '<br>';
}catch(Exception $e){
  echo $e->getMessage() . '<br>';
}

try{
  echo netmask2bitmask('ffff:ffff:ffff:ffc0:0:0:0:0') . '<br>';
}catch(Exception $e){
  echo $e->getMessage() . '<br>';
}
