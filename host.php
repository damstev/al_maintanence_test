<?php
include 'db.php';

class Host
{
    public static function getList($only_active = false)
    {
        /**
        * The method is caching the data the first time that is executed inside the same request lifecycle, returning the same (maybe wrong) data in subsequent calls.
        */
        static $result;

        if (!empty($result)) return $result;

        $stmt = "SELECT
                    hos.host_id,
                    hos.host_name,
                    hos.ip_address,
                    cre.username
                 FROM
                    hosts hos
                    LEFT JOIN credentials cre
   	                ON(hos.credential_id = cre.credential_id)
                 WHERE
                    hos.deleted       = 0";
        if ($only_active === true) {
            $stmt .= " AND hos.active = 1";
        }
        $result = DB::getAll($stmt);

        return $result;
    }
}
?>
